<?php get_header(); ?>
		<?php $args = array(
			'theme_location'  => 'fine-menu',
			'container'       => 'nav',
			'container_class' => 'block_nav',
			'menu_class'      => 'menu',
			'echo'            => true,  
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 1 );
		wp_nav_menu($args); ?>	
		<section class="content">
			<section class="content_wrapp">
				<div class="states">
					<h2>Результат поиска по запросу: <?php echo get_search_query(); ?></h2>
					<?php if(have_posts()) :?>
					<?php while(have_posts()) : the_post();?>
						<article class="main_post">
							<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php if ( has_post_thumbnail()) {
									the_post_thumbnail('thumb-size-4',
									array('alt' => get_the_title()));
								} else {echo '<img src="'.get_template_directory_uri().'/images/thumbs/200x136.png" alt="'.get_the_title().'"/>';}
								?>
							</a>
							<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							<p><?php get_substr($post->post_content, 29); ?></p>
							<div class="read_more">
								<a href="<?php the_permalink(); ?>" title="Читать далее...">Читать далее...</a>
							</div>
						</article>
					<?php endwhile; ?>
					<?php else: ?>
						<h3>Извините, по Вашему запросу ничего не найдено...</h3>
					<?php endif; ?>
					<?php if (function_exists('wp_corenavi')) wp_corenavi(); ?>
				</div>
			</section>
			<?php get_sidebar(); ?>
		</section>
	</div>
<?php get_footer(); ?>