<?php get_header(); ?>

		<?php $args = array(
			'theme_location'  => 'fine-menu',
			'container'       => 'nav',
			'container_class' => 'block_nav',
			'menu_class'      => 'menu',
			'echo'            => true,  
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 1 );
		wp_nav_menu($args); ?>	
		<section class="content">
			<section class="content_wrapp">
				<div class="states">
					<h2><?php echo get_cat_name((int)get_option($shortname.'news_block1')); ?></h2>
					<?php
						global $wp_query;
						$user_qyery = array('posts_per_page' => 4, 'cat' => (int)get_option($shortname.'news_block1'));
						$args = array_merge($wp_query->query_vars, $user_qyery);
						query_posts($args);
						$i = 1;
						while (have_posts()) : the_post(); ?>
							<?php if($i == 1): ?>
								<article class="big_state">
									<?php 
										if (has_post_thumbnail()) { the_post_thumbnail('thumb-size-1', array('alt' => get_the_title()));} 
										else {echo '<img src="'.get_template_directory_uri().'/images/thumbs/376x256.png" alt="'.get_the_title().'"/>';}
									?>
									<div class="about_news">
										<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									</div>
								</article>
							<?php else: ?>
								<article class="state">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-2',
												array('alt' => get_the_title())
											);} else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/71x71.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('d F'); ?></time>
								</article>
					<?php endif; $i++; endwhile; wp_reset_query(); ?>
				</div>
				<div class="states">
					<h2><?php echo get_cat_name((int)get_option($shortname.'news_block2')); ?></h2>
					<?php
						global $wp_query;
						$user_qyery = array('posts_per_page' => 4, 'cat' => (int)get_option($shortname.'news_block2'));
						$args = array_merge($wp_query->query_vars, $user_qyery);
						query_posts($args);
						$i = 1;
						while (have_posts()) : the_post(); ?>
							<?php if($i == 1): ?>
								<article class="last_post">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-3',
												array('alt' => get_the_title())
											);} else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/283x166.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</article>
							<?php else: ?>
								<article class="post_one">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-2',
												array('alt' => get_the_title())
											); } else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/71x71.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</article>
					<?php endif; $i++; endwhile; wp_reset_query(); ?>
				</div>
				<div class="states">
					<h2><?php echo get_cat_name((int)get_option($shortname.'news_block3')); ?></h2>
					<?php
						global $wp_query;
						$user_qyery = array('posts_per_page' => 4, 'cat' => (int)get_option($shortname.'news_block3'));
						$args = array_merge($wp_query->query_vars, $user_qyery);
						query_posts($args);
						$i = 1;
						while (have_posts()) : the_post(); ?>
							<?php if($i == 1): ?>
								<article class="last_post">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-3',
												array('alt' => get_the_title())
											);} else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/283x166.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</article>
							<?php else: ?>
								<article class="post_one">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-2',
												array('alt' => get_the_title())
											); } else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/71x71.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</article>
					<?php endif; $i++; endwhile; wp_reset_query(); ?>
				</div>
				<div class="states">
					<h2><?php echo get_cat_name((int)get_option($shortname.'news_block4')); ?></h2>
					<?php
						global $wp_query;
						$user_qyery = array('posts_per_page' => 4, 'cat' => (int)get_option($shortname.'news_block4'));
						$args = array_merge($wp_query->query_vars, $user_qyery);
						query_posts($args);
						$i = 1;
						while (have_posts()) : the_post(); ?>
							<?php if($i == 1): ?>
								<article class="last_post">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-3',
												array('alt' => get_the_title())
											);} else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/283x166.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</article>
							<?php else: ?>
								<article class="post_one">
									<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php if ( has_post_thumbnail()) {
											the_post_thumbnail('thumb-size-2',
												array('alt' => get_the_title())
											); } else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/71x71.png" alt="'.get_the_title().'"/>';}
										?>
									</a>
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								</article>
					<?php endif; $i++; endwhile; wp_reset_query(); ?>
				</div>
			</section>
			<?php get_sidebar(); ?>
		</section>
	</div>
<?php get_footer(); ?>