<?php 

if(file_exists(STYLESHEETPATH.'/features/class.post.metabox.php')){
	require_once(STYLESHEETPATH.'/features/class.post.metabox.php');
}

if(file_exists(STYLESHEETPATH.'/features/widgets/class.widgets.php')){
	require_once(STYLESHEETPATH.'/features/widgets/class.widgets.php');
}

class WPTeamPanel{
	
	public $themename;

	public $shortname;

	public $options = array();
	
	public $system;
	
	function __construct(){		
		$this->themename = 'kredits-online';
		$this->shortname = 'prn';
		$this->setOptions();
		add_action('admin_init', array(&$this, 'mytheme_add_init'));
		add_action('admin_menu', array(&$this, 'mytheme_add_admin'));
		if(get_option('shortname') != $this->shortname){
			foreach($this->options as $opt_group){
				foreach($opt_group as $opt){
					if(isset($opt['id']) && isset($opt['std'])){
						if(get_option($opt['id']))
							update_option($opt['id'], stripslashes( wp_filter_post_kses( addslashes($opt['std']))));
						else
							add_option($opt['id'], stripslashes( wp_filter_post_kses( addslashes($opt['std']))));
					}
				}
			}
			if(get_option('shortname') == '' || get_option('shortname') != $this->shortname)
				update_option('shortname', $this->system['shortname']);
			else
				add_option('shortname', $this->system['shortname']);
		}
	}

   /**
	* @param none
	* @return list of user options
	*/
	public function setOptions(){
		$this->system['shortname'] = $this->shortname;
		$options['general'] = array(		
			'setting_name' => array(
				'title'		=>	'Общие настройки',
				'name'		=>	'Общие настройки',
				'slug'		=>	'general',
				'parent'	=>	0
			),
			
			array(
				'name'	=>	'Выбирите категорию для вывода в первый новостной блок на главной',
				'desc'	=>	'Выбирите категорию для вывода в первый новостной блок на главной',
				'id'	=>	$this->shortname.'news_block1',
				'type'	=>	'selectcategory'
			),
			
			array(
				'name'	=>	'Выбирите категорию для вывода во второй новостной блок на главной',
				'desc'	=>	'Выбирите категорию для вывода во второй новостной блок на главной',
				'id'	=>	$this->shortname.'news_block2',
				'type'	=>	'selectcategory'
			),
			
			array(
				'name'	=>	'Выбирите категорию для вывода в третий новостной блок на главной',
				'desc'	=>	'Выбирите категорию для вывода в третий новостной блок на главной',
				'id'	=>	$this->shortname.'news_block3',
				'type'	=>	'selectcategory'
			),
			
			array(
				'name'	=>	'Выбирите категорию для вывода в четвертый новостной блок на главной',
				'desc'	=>	'Выбирите категорию для вывода в четвертый новостной блок на главной',
				'id'	=>	$this->shortname.'news_block4',
				'type'	=>	'selectcategory'
			),
			
			array(
				'name'	=>	'Копирайт',
				'desc'	=>	'Введите текст копирайта',
				'id'	=>	$this->shortname.'copyright',
				'type'	=>	'textarea'
			),
			
			array(
				'name'	=>	'Баннер в шапке',
				'desc'	=>	'Введите HTML-код баннера в шапке',
				'id'	=>	$this->shortname.'top_banner',
				'type'	=>	'textarea',
				'std'	=>	''
			),

		);
		
		$this->options = $options;
		global $options;
		return $options;
	}
		
	public function get_section_content($section){
		$content = "
		<div class='wpt_opts'>
		<form method='post' enctype='multipart/form-data'> 
			<input name='reset' type='submit' value='Сбросить' class='reset_button'/>  
			<input type='hidden' name='action' value='reset' />
			<input type='hidden' name='current_section' value='".$section."' />
		</form>
		<form method='post'>
		<input type='hidden' name='current_section' value='".$section."'/>
		<input type='hidden' name='action' value='save'/>
		<input name='save' type='submit' value='Сохранить' class='submit_button'/>
		";
		foreach ($this->options[$section] as $value){
			$description = (!empty($value['desc']))? $value['desc'] : '';
			switch ($value['type']){					
				case "blocktitle":
					$content .= "<h3 id=".$value['id']." class='bootom_dotted'>".
						$value['name']
					."</h3>";
					break;
					
				case "simple_text":
					$content .= "<div class='wpt_field_area rm_text'>
						<label for='".$value['id']."'>".$value['name']."</label>
						<div id='".$value['id']."' class='rm_prewiew'>".$value['std']."</div>
					</div>";
					break;
					
				case 'largetext':
					$v = (get_option($value['id'])!= "") ? stripslashes(get_option($value['id'])) : $value['std'];
					$content .= "<div class='wpt_field_area wpt_largetext ".$value['id']."'>
						<label for='".$value['id']."'>".$value['name']."</label>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "
						<input 
							name='".$value['id']."'
							id='".$value['id']."'
							type='text'
							class='largetext'
							value='".$v."'
						/>
					</div>";
					break;
					
				case 'textarea':
					$text_content = (get_option($value['id'])) ? stripslashes(get_option($value['id'])) : $value['std'];
					$content .= "<div class='wpt_field_area wpt_textarea ".$value['id']."'>
						<div class='label-wrap'><label for='".$value['id']."'>".$value['name']."</label></div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "
						<textarea 
							name='".$value['id']."'
							id='".$value['id']."'
							class='wpt_textarea_wrap'
							>".$text_content."</textarea>
					</div>";  
					break;
					
				case 'selectcategory':
					$content .= "<div class='wpt_field_area wpt_select_area ".$value['id']."'>
						<div class='label-wrap'><label for='".$value['id']."'>".$value['name']."</label></div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "
						<select class='wpt-select' name='".$value['id']."' id='".$value['id']."'>";
							$args = array(
								'type'                     => 'post',
								'orderby'                  => 'name',
								'order'                    => 'ASC',
								'hide_empty'               => 0,
							);
							$categories = get_categories($args);
							foreach ($categories as $option){
								$is_checked = (get_option($value['id'])  == $option->term_id) ? 'selected="selected"' : "";
								$content .= "<option value='".$option->term_id."' ".$is_checked.">".$option->name."</option>";
							}
						$content .= "</select>
					</div>";
					break;
					
				case 'selectpages':
					$content .= "<div class='wpt_field_area wpt_select_area ".$value['id']."'>
						<div class='label-wrap'><label for='".$value['id']."'>".$value['name']."</label></div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "<select class='wpt-select' name='".$value['id']."' id='".$value['id']."'>";
						$posts_array = get_posts('post_type=page');
						foreach ($posts_array as $post){
							$selected = '';
							if(get_option('theme_featured_post') == $post->ID) $selected = ' selected="selected"';
							$content .= '<option value="'.$post->ID.'"'.$selected.'>'.$post->post_title.'</option>';
						}
						$content .= "</select>
					</div>";
					break;
					
				case 'select':
					$content .= "<div class='wpt_field_area wpt_select_area ".$value['id']."'>
						<div class='label-wrap'><label for='".$value['id']."'>".$value['name']."</label></div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "
						<select class='wpt-select' name='".$value['id']."' id='".$value['id']."'>";
							foreach ($value['optns'] as $option){
								$is_checked = (get_option($value['id'])  == $option['value']) ? 'selected="selected"' : "";
								$content .= "<option value='".$option['value']."' ".$is_checked.">".$option['title']."</option>";
							}
						$content .= "</select>
					</div>";
					break;
					
				case 'file':
					$v = (get_option($value['id'])!= "") ? stripslashes(get_option($value['id'])) : $value['std'];
					$content .= "<div class='wpt_file_area wpt_file ".$value['id']."'>
						<div class='label-wrap'><label for='".$value['id']."'>".$value['name']."</label></div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "
						<div class='upload-area'>
							<input 
								class='image_fld'
								name='".$value['id']."'
								id='".$value['id']."'
								type='text'
								value='".$v."'
							/>
							<input id='upload_image_button_".$value['id']."' class='upld_btn' type='button' value='".__('Upload')."'/>
						</div>
					</div>";
					break;
					
				case 'sidebar_select':
					$content .= "<div class='wpt_field_area rm_select ".$value['id']."'>
						<label for='".$value['id']."'>".$value['name']."</label>
						<div class='help_icon' id='help_w".$value['id']."'>
							<div class='help_icon_helper'><div class='help_wrap' style='display:none'>
								<p>".$description."</p>
							</div></div>
						</div>";
						global $wp_registered_sidebars;
						$content .= "<select name='".$value['id']."' id='".$value['id']."'>";
							foreach ($wp_registered_sidebars as $sidebar){
								$is_checked = (get_option($value['id'])  == $sidebar['id']) ? 'selected="selected"' : "";
								$content .= "<option value='".$sidebar['id']."' ".$is_checked.">".$sidebar['name']."</option>";
							}
						$content .= "</select>";
					$content .= "</div>";
					break; 
					
				case 'select_image':
					$content .= "<div class='wpt_field_area rm_select ".$value['id']."'>
						<label for='".$value['id']."' style='margin-bottom: 0'>".$value['name']."</label><br style='clear: both'/>
						<div class='help_icon' id='help_w".$value['id']."'>
							<div class='help_icon_helper'><div class='help_wrap' style='display:none'>
								<p>".$description."</p>
							</div></div>
						</div>
						<div class='".$value['id']." select_image_div'>";
							$img = '';
							$current_value = '';
							foreach ($value['optns'] as $option){
								$is_checked = '';
								if(get_option($value['id'])  == $option['img_path']){
									$is_checked = 'class="selected_bg_image"';
									$current_value = $option['img_path'];
								} else {
									$is_checked = 'class="unselected_bg_image"';
								}
								$content .= "<div ".$is_checked." id='".$option['img_path']."'><img src='".get_bloginfo('template_url')."/images/backgrounds/".$option['img_thumb']."' alt=''/></div>";
							}
						$content .= "</div>";
						$content .= "<input type='hidden' value='".$current_value."' name='".$value['id']."' id='".$value['id']."'/>";
					$content .= "</div>";
					break;
				case "checkbox":
					$content .= "<div class='wpt_field_area wpt_checkbox ".$value['id']."'>
						<div class='label-wrap'>".$value['name']."</div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "<div class='checkboxgroup-wrap'>";
						$ch_checked = "";
						if(get_option($value['id'])){
							$ch_checked = "checked=\"checked\"";
						}
						$content .= "<div class='wpt-ch-group'>
							<label for='".$value['id']."'>".$value['label']."</label>
							<input type='checkbox' class='checkbox_class' name='".$value['id']."' id='".$value['id']."' ".$ch_checked."/>
						</div>";
						$content .= "
						</div>
					</div>";
					break;
				case "checkboxgroup":
					$content .= "<div class='wpt_field_area wpt_checkbox ".$value['id']."'>
						<div class='label-wrap'>".$value['name']."</div>";
						if(!empty($description)) $content .= '<p class="wpt-field-description">'.$description.'</p>';
						$content .= "<div class='checkboxgroup-wrap'>";
							foreach ($value['checks'] as $checkbox){
							echo "<pre>";
							var_dump(get_option($checkbox['id']));
							echo "</pre>";
							
								$ch_checked = "";
								if(get_option($checkbox['id'])){
									$ch_checked = " checked=\"checked\"";
								}
								$content .= "<div class='wpt-ch-group'>
									<label for='".$checkbox['id']."'>".$checkbox['title']."</label>
									<input type='checkbox' class='checkbox_class' name='".$checkbox['id']."' id='".$checkbox['id']."' ".$ch_checked."/>
								</div>";
							}
						$content .= "
						</div>
					</div>";
					break;
				case "sidebars_manager":
					$list = get_option($this->shortname.'_dinamic_sidebars_list');
					$content .= "<div class='rm_input ".$value['id']."'>";
					if(!empty($list)){
						$content .= "<table>";
						$i = 5;
						$widget_list = wp_get_sidebars_widgets();
						foreach($list as $sidebar){
							$desc = (!empty($widget_list["sidebar-$i"])) ? "Widget list: ".implode(", ", $widget_list["sidebar-$i"]) : __("This sidebar has no widgets");
							$content .= "<tr><td><span>".$sidebar."</span><div class='help_icon' id='help_wsidebar-".$i."' style='margin:0'>
							<div class='help_icon_helper'><div class='help_wrap' style='display:none;'>
								<p>".$desc."</p>
							</div></div>
							</div><div class='delete_fld' id='delete_sidebar-".$i."'></div><div class='edit_fld' id='edit_sidebar-".$i."' onclick=\"edit_sidebar('sidebar-".$i."', '".$sidebar."')\"></div></td></tr>";
							$i++;
						}
						$content .= "</table><div class='help_icon_helper edit_form_popup' style='display: none;'><div class='help_wrap' style=' width: 400px'><input type='text' style='width:205px; float:left;margin: 0;' id='edit_sidebar_new_name'/><input type='hidden' id='edit_sidebar_id'/><input type='hidden' id='edit_sidebar_old_name'/><input type='button' name='cancel_sidebar_button' style='margin: 0;' class='upld_btn' id='cancel_sidebar_button' onclick=\"deactivate_edit_sidebar_form()\" value='Cancel'/><input type='button' name='edit_sidebar_button' class='save_sidebars_btn' onclick=\"save_sidebar()\" id='edit_sidebar_button' value='Save'/></div></div>";
					} else {
						$content .= "<label style='width: 558px; cursor: default'>".__("You did not have create specify sidebars")."</label>";
					}
					$content .= "</div>";
					break;
			}
		}
		$content .= "<input name='save' type='submit' value='Сохранить' class='submit_bottom_button'/>
				</form>
				<form method='post' enctype='multipart/form-data'> 
					<input name='reset' type='submit' value='Сбросить' class='reset_bottom_button'/>  
					<input type='hidden' name='action' value='reset' />
					<input type='hidden' name='current_section' value='".$section."' />
				</form>
			</div> ";
		
		return $content;
	}
	
	
	
	public function get_page_fields($page){
		$file_dir=get_bloginfo('template_directory');?>
		<script type="text/javascript">
			$(document).ready(function(){
				$('input.checkbox_class, select.wpt-select').styler();
				_at = $.cookie('current_tab');
				if(_at == null) active_tab = 'tab_menu_1';
				else active_tab = _at;
				act_sel = "div#"+active_tab;
				$(act_sel).addClass('selected');
				_tc=	$.cookie('current_tab_number');
				if(_tc == null) nn = 1;
				else nn = _tc;
				tc = '#tab_content_'+nn;
				$(tc).show();

				//add tabs
			    $(".tabs .tab[id^=tab_menu]").click(function() {
			        var curMenu=$(this);
			        $(".tabs .tab[id^=tab_menu]").removeClass("selected");
			        curMenu.addClass("selected");
					id = $(this).attr('id');
					$.cookie('current_tab', id);
					switch(id){
						case "tab_menu_1": $.cookie('current_tab_number', 1); break;
						case "tab_menu_2": $.cookie('current_tab_number', 2); break;
						case "tab_menu_3": $.cookie('current_tab_number', 3); break;
						case "tab_menu_4": $.cookie('current_tab_number', 4); break;
						case "tab_menu_5": $.cookie('current_tab_number', 5); break;
						case "tab_menu_6": $.cookie('current_tab_number', 6); break;
						case "tab_menu_7": $.cookie('current_tab_number', 7); break;
						case "tab_menu_8": $.cookie('current_tab_number', 8); break;
					}
			        var index=curMenu.attr("id").split("tab_menu_")[1];
			        $(".curvedContainer .tabcontent").css("display","none");
			        $(".curvedContainer #tab_content_"+index).css("display","block");
			    });
			});
		</script>
		<div class="wpt-admin-panel">
			<div class="wpt-admin-header">
				<div class="wpt-admin-header-logo"></div>
				<div class="wpt-admin-header-options"><?php _e('Theme options ', $this->themename); ?><?php echo $this->themename; ?></div>
			</div>
			<div id="admin_work_wrap">
				<div class="tabscontainer">
					<div class="tabs">
					<?php $i=1; ?>
					<?php $content = array();?>
				     	<?php foreach($this->options as $opt): ?>
					         <div class="tab <?php if( $i == 1 )echo "first"; elseif($i == sizeof($this->options)) echo "last";?>" id="tab_menu_<?php echo $i; ?>">
					             <div class="link"><?php echo $opt['setting_name']['name']; ?></div>
					         </div>
					        <?php $content[]= "<div class='tabcontent' id='tab_content_".$i."'>". $this->get_section_content($opt['setting_name']['slug'])."</div>"; ?>
				     		<?php $i ++; ?>
						<?php endforeach; ?>
				    </div>
					<?php $i=1; ?>
				    <div class="curvedContainer">
				    	<?php foreach ($content as $one_tab):?>
				    		<?php echo $one_tab; ?>
				    	<?php endforeach; ?>
				    </div>
				</div>
			</div>
			<div class="wpt-footer">
				<div class="ft-image"></div>
			</div>
		</div>
		<?php
	}
	
	public function mytheme_add_admin() {
		if(isset($_POST['current_section'])){
			if($_POST['current_section'] == 'sedebar'){
				$sidebar_title = strip_tags($_POST['mon_sidebar_title']);
				$old_list = (array)get_option($this->shortname.'_dinamic_sidebars_list');
				$new_list = array();
				if(count($old_list) == 0){
					$new_list[] = $sidebar_title;
				} elseif(!empty($old_list) && !in_array($sidebar_title, $old_list)) {
					$new_list = $old_list;
					$new_list[] = $sidebar_title;
				} elseif(in_array($sidebar_title, $old_list)){
					$new_list = $old_list;
				}
				update_option($this->shortname.'_dinamic_sidebars_list', $new_list);
			} else {
				$arrIndex = $_POST['current_section'];
				if(isset($_POST['action']) && $_POST['action'] == 'save') {
					foreach ($this->options[$arrIndex] as $value) {
						if(isset($value['id']) && isset($_POST[ $value['id'] ]))
							update_option( $value['id'], $_POST[ $value['id'] ] ); 
					}
					foreach ($this->options[$arrIndex] as $value) {
						if(isset($value['id']) && isset($_POST[$value['id']])) {
							update_option($value['id'], $_POST[$value['id']]); 
						} else {
							if(isset($value['id']))
								delete_option( $value['id'] );
						}
					}
				} elseif(isset($_POST['action']) && $_POST['action'] == 'reset') {
					foreach ($this->options[$arrIndex] as $value) {
						if(isset($value['id']) && isset($value['id']) && isset($value['std'])) {
							update_option($value['id'], $value['std']);
						} elseif(isset($value['id']) && !isset($value['id'])) { 
							add_option($value['id'], $value['std']);
						}
					}
				}
			}
		}
		add_menu_page($this->themename, $this->themename, 'administrator', $this->shortname."_management", array(&$this, 'init_first_page'));
	}
	
	public function init_first_page(){
		$this->get_page_fields('general');
	}

	public function mytheme_add_init(){
		if (isset($_GET['page']) && $_GET['page'] == $this->shortname.'_management') {
			add_action('admin_print_scripts', array(&$this, 'my_admin_scripts'));
			add_action('admin_print_styles', array(&$this, 'my_admin_styles'));
		}
	}
	
	/*
	 * Theme scripts
	 */
	public function my_admin_scripts() {
		wp_enqueue_script(false, get_bloginfo('template_directory')."/features/admin.js", false, false);
	}

	/*
	 * Theme styles
	 */
	public function my_admin_styles() {
		wp_enqueue_style(false, get_bloginfo('template_directory')."/features/admin.css", false, "1.0", "all");
	}
} $WPTeamPanel = new WPTeamPanel();