<?php
class LastPostWidget extends WP_Widget {

	function __construct(){
		$widget_ops = array('description' => 'Виджет последних новостей');
		$this->WP_Widget('LastPostWidget', 'WPTeam | Последние новости', $widget_ops);
	}

	public function form($instance){
		$instance = wp_parse_args((array) $instance, array('kredits_title' => '', 'kredits_count' => 3));
		?>
			<label for='<?php echo $this->get_field_id('kredits_title'); ?>'>Заголовок виджета</label> 
			<input style="float:right; width: 100%" id='<?php echo $this->get_field_id('kredits_title'); ?>' name='<?php echo $this->get_field_name('kredits_title'); ?>' type='text' value="<?php echo attribute_escape($instance['kredits_title']); ?>"/>
			<br/>
			<label for='<?php echo $this->get_field_id('kredits_count'); ?>'>Число записей</label> 
			<input style="float:right; width: 100%" id='<?php echo $this->get_field_id('kredits_count'); ?>' name='<?php echo $this->get_field_name('kredits_count'); ?>' type='text' value="<?php echo attribute_escape($instance['kredits_count']); ?>"/>
			<br/><br/>
			<select name='<?php echo $this->get_field_name('kredits_categ'); ?>' id='<?php echo $this->get_field_id('kredits_categ'); ?>'>
			<?php 
					$args = array('hide_empty'=> 0);
					$categories = get_categories($args);
					echo  "<option value='0'>Выбрать категорию</option>";
					foreach ($categories as $option){
						$is_checked = (attribute_escape($instance['kredits_categ']) == $option->term_id) ? 'selected="selected"' : "";
						echo "<option value='".$option->term_id."' ".$is_checked.">".$option->name."</option>";
					}
			?>
			</select>
		<?php
	}

	public function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['kredits_count'] = $new_instance['kredits_count'];
		$instance['kredits_title'] = $new_instance['kredits_title'];
		$instance['kredits_categ'] = $new_instance['kredits_categ'];
		return $instance;
	}

	public function widget($args, $instance){
		extract($args, EXTR_SKIP);
		?>
			<div class="last_posts">
				<h2><?php echo $instance['kredits_title']; ?></h2>				
				<?php 
					$args = array(
						'numberposts'	=>	(int)$instance['kredits_count'],
						'category'		=>  (int)$instance['kredits_categ'],
						'orderby'		=>	'date',
						'order'			=>	'DESC',
						'post_type'		=>	'post',
						'post_status'	=>	'publish',
					);
					$posts_array = get_posts($args);
					$i = 1;
					foreach ($posts_array as $post): ?>
						<article class="post_one">
							<a class="post_image" href="<?php echo get_permalink($post->ID); ?>" title="<?php echo get_the_title($post->ID); ?>">
								<?php if (has_post_thumbnail($post->ID) ) {
									echo get_the_post_thumbnail($post->ID, 'thumb-size-2',
										array('title' => get_the_title($post->ID), 'alt' => $post->post_title)
									);}	else {echo '<img src="'.get_template_directory_uri().'/images/thumbs/71x71.png" alt="'.get_the_title($post->ID).'"/>';}
								?>
							</a>
							<h3><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo get_the_title($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a></h3>
						</article>
				<?php endforeach; $i++;	wp_reset_query(); ?>
			</div>
		<?php
	}
}