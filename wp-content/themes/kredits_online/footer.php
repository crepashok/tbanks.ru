	<footer>
		<!-- Сделано в студии WPTeam -->
		<?php global $shortname; ?>
		<div class="intro">
			<div class="vidget_area">
				<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widgetable-posts-footer')) :?>
				<?php endif; ?>
			</div>			
		</div>
		<div class="bottom_footer aligncenter">
			<div class="heart">
				<?php $args = array(
				  'theme_location'  => 'footer-menu',
				  'container'       => 'nav',
				  'menu_class'      => 'menu',
				  'echo'            => true,  
				  'fallback_cb'     => 'wp_page_menu',
				  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				  'depth'           => 1 );
				wp_nav_menu($args); ?>
				<div class="copyright"><?php echo get_option($shortname.'copyright'); ?></div>
				
				<!--LiveInternet counter--><script type="text/javascript"><!--
				document.write("<a href='http://www.liveinternet.ru/click' "+
				"target=_blank><img src='//counter.yadro.ru/hit?t12.2;r"+
				escape(document.referrer)+((typeof(screen)=="undefined")?"":
				";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
				screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
				";"+Math.random()+
				"' alt='' title='LiveInternet: показано число просмотров за 24"+
				" часа, посетителей за 24 часа и за сегодня' "+
				"border='0' width='88' height='31'><\/a>")
				//--></script><!--/LiveInternet-->
						<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=21436993&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/21436993/3_0_FFFFFFFF_FFFFFFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:21436993,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter21436993 = new Ya.Metrika({id:21436993,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21436993" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
			</div>
		</div>
		<?php wp_footer(); ?>
	</footer>


</body>
</html>