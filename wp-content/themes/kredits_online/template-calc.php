<?php /* Template Name: Кредитный калькулятор */ ?>
<?php get_header(); ?>
		<?php $args = array(
			'theme_location'  => 'fine-menu',
			'container'       => 'nav',
			'container_class' => 'block_nav',
			'menu_class'      => 'menu',
			'echo'            => true,  
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 1 );
		wp_nav_menu($args); ?>	
		<section class="content">
			<section class="content_wrapp">
				<section class="single_area online-calc">
					<?php if(have_posts()): the_post(); ?>
						<h1 class="page_title"><?php the_title(); ?></h1>
						<div class="calculator-wrap">
							<div class="header">
								<img width="200" height="200" alt="Кредитный калькулятор онлайн" src="http://tbanks.ru/wp-content/themes/kredits_online/images/calc.png" title="Кредитный калькулятор онлайн" class="alignright">
							</div>
							<form class="calc" name="calc">
								<p><input class="needCheck" type="text" name="sum" placeholder="сумма кредита" /></p>
								<p><input class="needCheck" type="text" name="term" placeholder="срок в месяцах" /></p>
								<p><input class="needCheck" type="text" name="rate" placeholder="годовой %" /></p>
								<p><input type="text" name="firstPayment" placeholder="первый взнос" /></p>
								<h1>Тип платежей</h1>
								<p>
									<input type="radio" checked="checked" name="paymentType" value="annuitet" id="annuitet" />
									<label for="annuitet">Все одинаковые (аннуитетные)</label>
								</p>
								<p>
									<input type="radio" name="paymentType" value="differ" id="differ" />
									<label for="differ">Уменьшающиеся каждый месяц (Дифференциация)</label>
								</p>
								<p><input type="button" value="Расчёт" id="calc-go" /></p>
							</form>
							<div id="report"></div>
						</div>
						<?php the_content(); ?>
					<?php endif; ?>
				</section>
				<section class="single_area">
<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus,lj"></div> 
				</section>
			</section>
			<?php get_sidebar(); ?>
		</section>
	</div>
<?php get_footer(); ?>