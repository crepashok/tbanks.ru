/**
  1.05
*/
(
  function (toolboxName) {
    var toolsObj, x;
    
    window[toolboxName] = function (query, all) {
      return all ? document.querySelectorAll(query)
        : document.querySelector(query)
      ;
    };
    
     toolsObj = {
       /**
          Получить значение или несколько значений. Принимает CSS-селектор.
        Если нужны все значения, вторым параметром передаём true.
          Если нужен только текст (не innerHTML) для объектов без value,
        третьим параметром передаём true.
        
      */
       val: (
         function() {
           function grab(el, onlyText) {
            return el instanceof Object 
                ? "value" in el 
                 ? el.value
                 : onlyText
                   ? el.innerText
                   : el.innerHTML
               : void(0)
             ; 
           };
           
          return function(txt, all, onlyText) {
             var x, el = window[toolboxName](txt, all), result;
          
            if(all) {
             result = [];
              for(x = 0; x < el.length; x++) {
                result.push(grab(el[x], onlyText));
              }
            } else {
              result = grab(el, onlyText);
            }
           
             return result;
           }
         }
       )(),
       
      /** 
        Возвращает значения value в массиве для 
        элементов с checked = true из списка всех
        с указанным именем.
      */
      getCheckedValues: function(name) {
        var elements = document.getElementsByName(name), result = [ ];
        for(x = 0; x < elements.length; x++) {
          if(elements[x].checked) result.push(elements[x].value);
        }
        return result;
      },
      
      toBorn: function(nodeName, values, parentNode){
        var x, element = document.createElement(nodeName);
        values = values || {};
        for(x in values){
          element[x] = values[x];
        }
        if(parentNode) {
          parentNode.appendChild(element);
        }
        return element;
      },
      
      getPos: function(element) {
        var result = {x: 0, y: 0};
        if(!element) return result;
  
        while("offsetTop" in element) {
          result.x += element.offsetLeft;
          result.y += element.offsetTop;
          element = element.parentNode;
        }
  
        return result
      }
      
    }//tools object
    
    for(x in toolsObj) {
      window[toolboxName][x] = toolsObj[x];
    }
  }// анонимная функция
  
)("gc")

