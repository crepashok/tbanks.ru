<?php



require_once(ABSPATH . 'wp-settings.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );



//if (file_exists(dirname( __FILE__ ) . '/ios-uuid-manager.php'))
require_once( Wordpress_iOS_Plugin_FILE . 'ios-uuid-manager.php' );



class ZaymyUkraineTaxonomyManager {


    private $jsonContentFile;


    public $addCustomActionPriority = 10;


    public $addCustomActionAcceptArgs = 3;


    function __construct() {

        $this->jsonContentFile = ABSPATH . "mobile-zu.json";

    }


    public function initTaxonomyCustomStyles() {

        if ($_GET['taxonomy'] && $_GET['taxonomy'] == ZaymyUkraineTaxonomyName) {

            wp_enqueue_style('font-awesome', plugins_url( 'css/font-awesome.min.css', Wordpress_ZaymyUkraine_Plugin_FILE ) );

        }
    }


    public function initCustomPostTypeAndTaxonomy() {

        $labels = array(
            'name'               => 'Записи Займы Украины',
            'singular_name'      => 'Запись Займ Украины',
            'menu_name'          => 'Займы Украины',
            'name_admin_bar'     => 'Запись Займ Украины',
            'add_new'            => 'Добавить запись',
            'add_new_item'       => 'Новая запись',
            'new_item'           => 'Новая запись',
            'edit_item'          => 'Редактировать запись',
            'view_item'          => 'Просмотр записи',
            'all_items'          => 'Все записи',
            'search_items'       => 'Поиск записи',
            'parent_item_colon'  => 'Родительская запись:',
            'not_found'          => 'Записей не найдено',
            'not_found_in_trash' => 'Записей в корзине не найдено'
        );

        $args = array(
            'labels'             => $labels,
            'description'        => 'На сайте данный тип записи не будет отображаться.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => ZaymyUkrainePostTypeName ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => 6,
            'supports'           => array( 'title', 'editor', 'thumbnail' )
        );

        register_post_type( ZaymyUkrainePostTypeName, $args );


        $labels = array(
            'name'              => 'Раздел Займ Украины',
            'singular_name'     => 'Раздел Займ Украины',
            'search_items'      => 'Поиск разделов',
            'all_items'         => 'Все разделы',
            'parent_item'       => 'Родительский раздел',
            'parent_item_colon' => 'Родительский раздел',
            'edit_item'         => 'Редактировать раздел',
            'update_item'       => 'Редактировать раздел',
            'add_new_item'      => 'Новый раздел',
            'new_item_name'     => 'Новый раздел',
            'menu_name'         => 'Разделы',
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => ZaymyUkraineTaxonomyName ),
        );

        register_taxonomy( ZaymyUkraineTaxonomyName, array( ZaymyUkrainePostTypeName ), $args );

        flush_rewrite_rules();
    }


    public function updateCustomPostType( $post_id, $post, $update ) {

        if ($post->post_type != ZaymyUkrainePostTypeName) {
            return;
        }

        $isPushEnabled = (function_exists("get_field")) ? get_field('send_push_notification', $post->ID) : false;

        if ($isPushEnabled) {

            $manager = new ZaymyUkraineUUIDManager();

            $postMeta = get_post_meta( $post->ID );

            $manager->sendPushNotificationWithText($post->ID, ($postMeta["push_notification_text"][0]) ? $postMeta["push_notification_text"][0] : "");
        }

        $this->regenerateJSONData();
    }


    public function updateCustomTaxonomy( $taxonomy ){

        $this->regenerateJSONData();

    }


    private function regenerateJSONData() {

        $jsonContent = array (
            "updatedAt" => date("Y-m-d H:i:s"),
            "content"   => $this->getTaxonomiesContent()
        );

        $file = fopen($this->jsonContentFile,"w");

        fwrite($file, json_encode($jsonContent));

        fclose($file);
    }


    private function getTaxonomiesContent() {

        $categories = get_terms(
            ZaymyUkraineTaxonomyName,
            array(
                'orderby'    => 'count',
                'hide_empty' => 0
            )
        );

        $jsonTerms = array();

        if (count($categories) > 0) {

            foreach ($categories as $category) {

                $postItems = get_posts(
                    array(
                        'post_type'     => ZaymyUkrainePostTypeName,
                        'numberposts'   => -1,
                        'tax_query'     => array(
                            array(
                                'taxonomy'  => ZaymyUkraineTaxonomyName,
                                'field'     => 'term_id',
                                'terms'     => $category->term_id
                            )
                        )
                    )
                );

                $jsonPosts = array();

                if (count($postItems) > 0) {

                    foreach ( $postItems as $post ) {

                        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 180, 100) );

                        $largeImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 280, 156) );

                        $postMeta = get_post_meta( $post->ID );

                        $jsonPosts[] = array(
                            "bankid"        => $post->ID,
                            "title"         => $post->post_title,
                            "status"        => $post->post_status,
                            "url"           => get_permalink($post->ID),
                            "content"       => $post->post_content,
                            "thumb_url"     => (!$thumbnail[0]) ? "" : $thumbnail[0],
                            "image_url"     => (!$largeImage[0]) ? "" : $largeImage[0],
                            "max_amount"    => (float)($postMeta["max_money"][0]) ? $postMeta["max_money"][0] : "",
                            "term"          => ($postMeta["term"][0]) ? $postMeta["term"][0] : "",
                            "rate"          => ($postMeta["precentage"][0]) ? $postMeta["precentage"][0] : "",
                            "rating"        => (float)($postMeta["rate"][0]) ? $postMeta["rate"][0] : 0,
                            "referal_link"  => ($postMeta["referal_link"][0]) ? $postMeta["referal_link"][0] : "",
                            "money_method"  => ($postMeta["get_money_method"][0]) ? $postMeta["get_money_method"][0] : "",
                            "updated_at"    => $post->post_modified
                        );

                    }
                }

                $jsonTerms[] = array(
                    "title"       => $category->name,
                    "category_id" => $category->term_id,
                    "url"         => get_term_link($category),
                    "items"       => $jsonPosts,
                    "icon"        => (function_exists("get_field")) ? get_field('category_icon', ZaymyUkraineTaxonomyName."_".$category->term_id) : "",
                    "position"    => (function_exists("get_field")) ? (int)get_field('category_position', ZaymyUkraineTaxonomyName."_".$category->term_id) : 0
                );
            }
        }

        return $jsonTerms;
    }
}



$taxZaymyUkraineManager = new ZaymyUkraineTaxonomyManager();

add_action( 'init', array( &$taxZaymyUkraineManager, 'initCustomPostTypeAndTaxonomy' ) );

add_action( 'admin_menu', array( &$taxZaymyUkraineManager, 'initTaxonomyCustomStyles' ) );

add_action( 'save_post', array( &$taxZaymyUkraineManager, 'updateCustomPostType' ), $taxZaymyUkraineManager->addCustomActionPriority, $taxZaymyUkraineManager->addCustomActionAcceptArgs );

add_action( 'create_'.ZaymyUkraineTaxonomyName, array( &$taxZaymyUkraineManager, 'updateCustomTaxonomy'), $taxZaymyUkraineManager->addCustomActionPriority, $taxZaymyUkraineManager->addCustomActionAcceptArgs );

add_action( 'edit_'.ZaymyUkraineTaxonomyName, array( &$taxZaymyUkraineManager, 'updateCustomTaxonomy'), $taxZaymyUkraineManager->addCustomActionPriority, $taxZaymyUkraineManager->addCustomActionAcceptArgs );

add_action( 'delete_'.ZaymyUkraineTaxonomyName, array( &$taxZaymyUkraineManager, 'updateCustomTaxonomy'), $taxZaymyUkraineManager->addCustomActionPriority, $taxZaymyUkraineManager->addCustomActionAcceptArgs );