<?php

class IosRestApi {

    function __construct() {

        add_action( 'rest_api_init', function () {
            register_rest_route( 'ios/v1', '/author/(?P<id>\d+)', array(
                'methods' => 'GET',
                'callback' => array( &$this, 'getUserData' ),
            ) );
        } );

    }

    function getUserData( $data ) {
        $posts = get_posts( array(
            'author' => $data['id'],
        ) );

        if ( empty( $posts ) ) {
            return null;
        }

        return $posts[0]->post_title;
    }


}

$rest = new IosRestApi();