<?php

require_once(ABSPATH . 'wp-settings.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );


class iOSOptions {

    public $tableName;

    private $wpDBContext;

    private $socketConnection;

    private $devicesURLKey;

    private $settingsURLKey;

    private $pemFileExtension = "pem";

    private $prefix;

    private $appplicationName;

    private $pemFileOptionKey;

    private $pemFilePassphraseOptionKey;

    private $pemFilePassphraseDefaultValue;

    private $postType;

    private $testDevice = null; //fa72d4025b6dbe5bae958f24df59a7c8fe6c2df329a0c5f1a23a831ce469cc56

    private $sslPath = 'ssl://gateway.push.apple.com:2195';             //live
//    private $sslPath = 'ssl://gateway.sandbox.push.apple.com:2195';   //demo


    function __construct($prefix, $appplicationName) {

        global $wpdb;

        $this->prefix = $prefix;
        $this->postType = $this->prefix . "-" . ZUPostTypeName;
        $this->settingsURLKey = $this->getPrefix() . "config";
        $this->devicesURLKey = $this->getPrefix() . "devices";
        $this->appplicationName = $appplicationName;
        $this->pemFileOptionKey = $this->prefix . "_pem_file_option_key";
        $this->pemFilePassphraseOptionKey = $this->prefix . "_pem_file_passphrase_option_key";
        $this->pemFilePassphraseDefaultValue = "";

        $this->tableName = $wpdb->prefix . "uuid_items";
        $this->wpDBContext = $wpdb;
    }


    private function getPrefix() {

        return ($this->prefix) ? $this->prefix."-" : "";

    }



    public function getPassPhrase() {

        return (get_option($this->pemFilePassphraseOptionKey)) ? get_option($this->pemFilePassphraseOptionKey) : $this->pemFilePassphraseDefaultValue;

    }



    public function getPemFileName() {

        return get_option($this->pemFileOptionKey);

    }



    private function getPemFileDir() {

        $folderPath = Wordpress_iOS_Plugin_Folder . "/pem/" .$this->prefix."/";

        if (!file_exists($folderPath)) {
            mkdir($folderPath, 0777, true);
        }

        return $folderPath;
    }



    public function getPemFileFullPath() {

        $fullFilePath = $this->getPemFileDir() . $this->getPemFileName();

        return ($this->getPemFileName() && (file_exists($fullFilePath))) ? $fullFilePath : null;

    }



    public function updatePluginOptionsWithPostData($postData, $fileData) {

        update_option($this->pemFilePassphraseOptionKey, esc_attr($postData[$this->pemFilePassphraseOptionKey]));

        if ($fileData[$this->pemFileOptionKey]['size']) {

            $filename = $fileData[$this->pemFileOptionKey]['name'];

            if( pathinfo($filename, PATHINFO_EXTENSION) != $this->pemFileExtension ) {

                wp_die("Не верный формат файла");

            }

            if (file_exists($this->getPemFileFullPath())) {

                unlink($this->getPemFileFullPath());

            }

            update_option($this->pemFilePassphraseOptionKey, esc_attr($postData[$this->pemFilePassphraseOptionKey]));

            $target_path = $this->getPemFileDir(). basename( $filename );

            if(move_uploaded_file($fileData[$this->pemFileOptionKey]['tmp_name'], $target_path)) {

                update_option($this->pemFileOptionKey, esc_attr($filename));

            } else {

                delete_option($this->pemFileOptionKey);

            }
        }
    }



    public function linkCustomStylesAndScriptsToDashboard() {

        if ($_GET['page'] && $_GET['page'] == $this->devicesURLKey) {

            wp_enqueue_style('data-table-css',          plugins_url( 'css/jquery.dataTables.min.css', Wordpress_iOS_Plugin_FILE ) );
            wp_enqueue_style('data-table-css-sh-core',  plugins_url( 'css/dataTables.bootstrap.min.css', Wordpress_iOS_Plugin_FILE ));
            wp_enqueue_style('font-awesome',  plugins_url( 'css/font-awesome.min.css', Wordpress_iOS_Plugin_FILE ));
            wp_enqueue_script('data-table-jquery',     plugins_url( 'js/jquery-1.12.0.min.js', Wordpress_iOS_Plugin_FILE ));
            wp_enqueue_script('data-table-js',         plugins_url( 'js/jquery.dataTables.min.js', Wordpress_iOS_Plugin_FILE ));
        }
    }



    private function createUUIDTable() {

        if($this->wpDBContext->get_var("show tables like '{$this->tableName}'") != $this->tableName) {
            $sql = "CREATE TABLE {$this->tableName} (
                    `id` mediumint(9) NOT NULL AUTO_INCREMENT,
                    `token` varchar(100) DEFAULT '' NOT NULL,
                    `uuid` varchar(100) DEFAULT '' NOT NULL,
                    `device` varchar(255) DEFAULT '' NOT NULL,
                    `version` varchar(255) DEFAULT '' NOT NULL,
                    `name` varchar(255) NOT NULL,
                    `app` varchar(255) NOT NULL,
                    `createdAt` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    `updatedAt` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    UNIQUE KEY id (id)
                    );";
            dbDelta( $sql );
        }
    }



    private function strReplaceAssoc(array $replace, $subject) {

        return str_replace(array_keys($replace), array_values($replace), $subject);

    }



    public function createNewUUID($_uuid, $_token, $_device, $_name, $_version, $_bundle) {

        $replaceDict = array(
            '<' => '',
            '>' => '' ,
            ' ' => ''
        );

        $uuid = mysql_real_escape_string($_uuid);

        $token = mysql_real_escape_string($this->strReplaceAssoc($replaceDict, $_token));

        $device = mysql_real_escape_string($_device);

        $version = mysql_real_escape_string($_version);

        $name = mysql_real_escape_string($_name);

        $bundle = mysql_real_escape_string($_bundle);

        if (!strlen($uuid) || !strlen($device) || !strlen($name) || !strlen($bundle)) {

            return;

        }

        $dbUUIData = $this->wpDBContext->get_row("SELECT id FROM ".$this->tableName." WHERE uuid = '".$uuid."' AND app = '".$bundle."'");

        var_dump($this->tableName);

        if (!$dbUUIData) {

            $this->wpDBContext->insert(
                $this->tableName,
                array(
                    'createdAt' => current_time( 'mysql' ),
                    'updatedAt' => current_time( 'mysql' ),
                    'token'     => $token,
                    'uuid'      => $uuid,
                    'device'    => $device,
                    'version'   => $version,
                    'name'      => $name,
                    'app'       => $bundle
                )
            );

        } else {

            $this->wpDBContext->update(
                $this->tableName,
                array(
                    'updatedAt' => current_time( 'mysql' ),
                    'token'     => $token,
                    'uuid'      => $uuid,
                    'device'    => $device,
                    'version'   => $version,
                    'name'      => $name,
                    'app'       => $bundle
                ),
                array( 'id' => $dbUUIData->id )
            );
        }
    }



    public function getUUIDCollection() {

        return $this->wpDBContext->get_results("SELECT * FROM " . $this->tableName." WHERE app = '".$this->prefix."'");

    }



    public function sendPushNotificationWithText($postID, $message) {

        $allDevices = $this->getUUIDCollection();

        if (!empty($this->testDevice)) {

            $this->sendPushToDevice($postID, $this->testDevice, $message);

        } else {

            if (count($allDevices) > 0) {

                foreach ($allDevices as $device) {

                    $this->sendPushToDevice($postID, $device->token, $message);

                }
            }
        }
    }



    private function sendPushToDevice($postID = 0, $deviceToken, $message, $displayResult = false) {


        if ($this->getPemFileFullPath() && $this->getPassPhrase()) {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $this->getPemFileFullPath());
            stream_context_set_option($ctx, 'ssl', 'passphrase', $this->getPassPhrase());

            $this->socketConnection = stream_socket_client($this->sslPath, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$this->socketConnection) {
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            }
        }

        $message = iconv(mb_detect_encoding($message), "UTF-8", $message);

        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'code'  => 0,
            'post'  => (int)$postID
        );

        $payload = json_encode($body);

        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        $result = fwrite($this->socketConnection, $msg, strlen($msg));

        if ($displayResult) {

            echo  (!$result) ? 'Message not delivered' . PHP_EOL : 'Message successfully delivered' . PHP_EOL;

        }

        fclose($this->socketConnection);
    }



    public function initUUIDManagerConfigurationPage() {
        add_submenu_page( 'edit.php?post_type='.$this->postType, 'Устройства приложения '.$this->appplicationName, 'Устройства', 'manage_options', $this->devicesURLKey, array( &$this, 'pushNotificationDevicesPage' ) );
        add_submenu_page( 'edit.php?post_type='.$this->postType, 'Настройки приложения '.$this->appplicationName, 'Настройки', 'manage_options', $this->settingsURLKey, array( &$this, 'pushNotificationSettingPage' ) );
    }



    public function pushNotificationDevicesPage() {

        $UUIDs = $this->getUUIDCollection();

        $resultTable = "<div class='wrap'><h1>Устройства приложения ".$this->appplicationName."</h1><br/>";

        $resultTable .= "<table id='uuid-table' class='display table table-striped' cellspacing='0' width='100%'>";

        $resultTable .= "<thead><tr><th>Имя</th><th>Тип</th><th>Версия</th><th>Токен</th><th>Дата обновления</th><th></th></tr></thead>";

        $resultTable .= "<tbody>";

        if (count($UUIDs) > 0) {

            foreach ( $UUIDs as $item ) {

                $resultTable .= "<tr>
                <td class='td-name'>{$item->name}</td>
                <td class='td-device'>{$item->device}</td>
                <td class='td-version'>{$item->version}</td>
                <td class='td-uuid'>{$item->token}</td>
                <td class='td-updated-time'>{$item->updatedAt}</td>
                <td class='td-actions'><button class='delete-device-btn' data-uuid='{$item->uuid}'><i class='fa fa-chain-broken' aria-hidden='true'></i></button></td>
                </tr>";

            }
        }

        $resultTable .= "<tbody></table>";

        $resultTable .= "
        <script>
            $(document).ready(function() {
                $('#uuid-table').DataTable();

                $('#uuid-table').on('click', '.delete-device-btn', function(){

                    console.log('delete row!!!');

                    var deviceUUID = $(this).data('uuid'),
                        parentTR = $(this).closest('tr'),
                        ajaxUrl = '".admin_url('admin-ajax.php')."';

                    $.ajax({
                        type: 'GET',
                        url: ajaxUrl,
                        data: {
                            action: 'deletetoken',
                            prefix: '".$this->prefix."',
                            uuid :  deviceUUID
                        },
                        success: function (response) {
                            console.log(response);
                            parentTR.remove();
                        }
                    });
                })
            } );
        </script></div>";

        echo $resultTable;
    }


    public function deletetoken_action_callback() {

        $action = $_GET['action'];

        if ($action == 'deletetoken' && $_GET['prefix'] == $this->prefix) {

            $uuid = $_GET['uuid'];

            $uuidArr = array('uuid' => $uuid, 'app' => $this->prefix);

            $this->wpDBContext->delete($this->tableName, $uuidArr);

            print json_encode($uuidArr);
        }

        return;
    }



    public function pushNotificationSettingPage() {
        ?>
        <div class='wrap'>
            <h1>Настройки приложения <?php echo $this->appplicationName ?></h1><br/>
            <form action="edit.php?post_type=<?php echo $this->postType; ?>&page=<?php echo $this->settingsURLKey; ?>" method="post" enctype="multipart/form-data">
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row">
                            <label for="<?php echo $this->pemFileOptionKey; ?>">Upload PEM file</label>
                        </th>
                        <td>
                            <input type="file"
                                   id="<?php echo $this->pemFileOptionKey; ?>"
                                   name="<?php echo $this->pemFileOptionKey; ?>"
                                />
                            <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
                            <p class="description" id="tagline-description">

                                Actual file: <?php echo $this->getPemFileFullPath(); ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="<?php echo $this->pemFilePassphraseOptionKey; ?>">PEM file Passphrase</label>
                        </th>
                        <td>
                            <input type="text"
                                   id="<?php echo $this->pemFilePassphraseOptionKey; ?>"
                                   name="<?php echo $this->pemFilePassphraseOptionKey; ?>"
                                   value="<?php echo $this->getPassPhrase(); ?>"
                                />
                            <p class="description" id="tagline-description"></p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <input type="submit" value="Сохранить" name="<?php echo $this->prefix ?>_update_ios_settings" class="button button-primary button-large"/>
                        </th>
                        <td>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    <?php }



    public function configureData() {
        $this->createUUIDTable();
        add_action( 'admin_menu', array( &$this, 'initUUIDManagerConfigurationPage' ) );
        add_action( 'admin_enqueue_scripts', array( &$this, 'linkCustomStylesAndScriptsToDashboard' ) );
        add_action( 'wp_ajax_deletetoken', array( &$this, 'deletetoken_action_callback' ));
//        add_action( 'wp_ajax_nopriv_deletetoken', array( &$this, 'deletetoken_action_callback' ));
    }
}

